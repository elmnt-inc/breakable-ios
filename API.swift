//  This file was automatically generated and should not be edited.

import AWSAppSync

public struct CreateGameInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: Int, createdAt: Int? = nil, updatedAt: Int? = nil, createdBy: String, startTime: Int? = nil, endTime: Int? = nil) {
    graphQLMap = ["id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime]
  }

  public var id: Int {
    get {
      return graphQLMap["id"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var createdAt: Int? {
    get {
      return graphQLMap["createdAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var updatedAt: Int? {
    get {
      return graphQLMap["updatedAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "updatedAt")
    }
  }

  public var createdBy: String {
    get {
      return graphQLMap["createdBy"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdBy")
    }
  }

  public var startTime: Int? {
    get {
      return graphQLMap["startTime"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startTime")
    }
  }

  public var endTime: Int? {
    get {
      return graphQLMap["endTime"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "endTime")
    }
  }
}

public struct UpdateGameInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: Int, createdAt: Int? = nil, updatedAt: Int? = nil, createdBy: String? = nil, startTime: Int? = nil, endTime: Int? = nil) {
    graphQLMap = ["id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime]
  }

  public var id: Int {
    get {
      return graphQLMap["id"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var createdAt: Int? {
    get {
      return graphQLMap["createdAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var updatedAt: Int? {
    get {
      return graphQLMap["updatedAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "updatedAt")
    }
  }

  public var createdBy: String? {
    get {
      return graphQLMap["createdBy"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdBy")
    }
  }

  public var startTime: Int? {
    get {
      return graphQLMap["startTime"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "startTime")
    }
  }

  public var endTime: Int? {
    get {
      return graphQLMap["endTime"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "endTime")
    }
  }
}

public struct CreateUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: String? = nil, createdAt: Int? = nil, updatedAt: Int? = nil, handle: String, profileS3Object: String? = nil) {
    graphQLMap = ["id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object]
  }

  public var id: String? {
    get {
      return graphQLMap["id"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var createdAt: Int? {
    get {
      return graphQLMap["createdAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var updatedAt: Int? {
    get {
      return graphQLMap["updatedAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "updatedAt")
    }
  }

  public var handle: String {
    get {
      return graphQLMap["handle"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "handle")
    }
  }

  public var profileS3Object: String? {
    get {
      return graphQLMap["profileS3Object"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "profileS3Object")
    }
  }
}

public struct UpdateUserInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: String, createdAt: Int? = nil, updatedAt: Int? = nil, handle: String? = nil, profileS3Object: String? = nil) {
    graphQLMap = ["id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object]
  }

  public var id: String {
    get {
      return graphQLMap["id"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var createdAt: Int? {
    get {
      return graphQLMap["createdAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var updatedAt: Int? {
    get {
      return graphQLMap["updatedAt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "updatedAt")
    }
  }

  public var handle: String? {
    get {
      return graphQLMap["handle"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "handle")
    }
  }

  public var profileS3Object: String? {
    get {
      return graphQLMap["profileS3Object"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "profileS3Object")
    }
  }
}

public struct CreateUserGameInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: String, gameId: Int) {
    graphQLMap = ["userId": userId, "gameId": gameId]
  }

  public var userId: String {
    get {
      return graphQLMap["userId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var gameId: Int {
    get {
      return graphQLMap["gameId"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gameId")
    }
  }
}

public struct UpdateUserGameInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(userId: String, gameId: Int) {
    graphQLMap = ["userId": userId, "gameId": gameId]
  }

  public var userId: String {
    get {
      return graphQLMap["userId"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "userId")
    }
  }

  public var gameId: Int {
    get {
      return graphQLMap["gameId"] as! Int
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gameId")
    }
  }
}

public final class DeleteGameMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteGame($id: Int!) {\n  deleteGame(id: $id) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    createdBy\n    startTime\n    endTime\n  }\n}"

  public var id: Int

  public init(id: Int) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteGame", arguments: ["id": GraphQLVariable("id")], type: .object(DeleteGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteGame: DeleteGame? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteGame": deleteGame.flatMap { $0.snapshot }])
    }

    public var deleteGame: DeleteGame? {
      get {
        return (snapshot["deleteGame"] as? Snapshot).flatMap { DeleteGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteGame")
      }
    }

    public struct DeleteGame: GraphQLSelectionSet {
      public static let possibleTypes = ["Game"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdBy", type: .nonNull(.scalar(String.self))),
        GraphQLField("startTime", type: .scalar(Int.self)),
        GraphQLField("endTime", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: Int, createdAt: Int, updatedAt: Int, createdBy: String, startTime: Int? = nil, endTime: Int? = nil) {
        self.init(snapshot: ["__typename": "Game", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: Int {
        get {
          return snapshot["id"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var createdBy: String {
        get {
          return snapshot["createdBy"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdBy")
        }
      }

      public var startTime: Int? {
        get {
          return snapshot["startTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: Int? {
        get {
          return snapshot["endTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "endTime")
        }
      }
    }
  }
}

public final class CreateGameMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateGame($createGameInput: CreateGameInput!) {\n  createGame(createGameInput: $createGameInput) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    createdBy\n    startTime\n    endTime\n  }\n}"

  public var createGameInput: CreateGameInput

  public init(createGameInput: CreateGameInput) {
    self.createGameInput = createGameInput
  }

  public var variables: GraphQLMap? {
    return ["createGameInput": createGameInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createGame", arguments: ["createGameInput": GraphQLVariable("createGameInput")], type: .object(CreateGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createGame: CreateGame? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createGame": createGame.flatMap { $0.snapshot }])
    }

    public var createGame: CreateGame? {
      get {
        return (snapshot["createGame"] as? Snapshot).flatMap { CreateGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createGame")
      }
    }

    public struct CreateGame: GraphQLSelectionSet {
      public static let possibleTypes = ["Game"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdBy", type: .nonNull(.scalar(String.self))),
        GraphQLField("startTime", type: .scalar(Int.self)),
        GraphQLField("endTime", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: Int, createdAt: Int, updatedAt: Int, createdBy: String, startTime: Int? = nil, endTime: Int? = nil) {
        self.init(snapshot: ["__typename": "Game", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: Int {
        get {
          return snapshot["id"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var createdBy: String {
        get {
          return snapshot["createdBy"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdBy")
        }
      }

      public var startTime: Int? {
        get {
          return snapshot["startTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: Int? {
        get {
          return snapshot["endTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "endTime")
        }
      }
    }
  }
}

public final class UpdateGameMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateGame($updateGameInput: UpdateGameInput!) {\n  updateGame(updateGameInput: $updateGameInput) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    createdBy\n    startTime\n    endTime\n  }\n}"

  public var updateGameInput: UpdateGameInput

  public init(updateGameInput: UpdateGameInput) {
    self.updateGameInput = updateGameInput
  }

  public var variables: GraphQLMap? {
    return ["updateGameInput": updateGameInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateGame", arguments: ["updateGameInput": GraphQLVariable("updateGameInput")], type: .object(UpdateGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateGame: UpdateGame? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateGame": updateGame.flatMap { $0.snapshot }])
    }

    public var updateGame: UpdateGame? {
      get {
        return (snapshot["updateGame"] as? Snapshot).flatMap { UpdateGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateGame")
      }
    }

    public struct UpdateGame: GraphQLSelectionSet {
      public static let possibleTypes = ["Game"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdBy", type: .nonNull(.scalar(String.self))),
        GraphQLField("startTime", type: .scalar(Int.self)),
        GraphQLField("endTime", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: Int, createdAt: Int, updatedAt: Int, createdBy: String, startTime: Int? = nil, endTime: Int? = nil) {
        self.init(snapshot: ["__typename": "Game", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: Int {
        get {
          return snapshot["id"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var createdBy: String {
        get {
          return snapshot["createdBy"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdBy")
        }
      }

      public var startTime: Int? {
        get {
          return snapshot["startTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: Int? {
        get {
          return snapshot["endTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "endTime")
        }
      }
    }
  }
}

public final class DeleteUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteUser($id: String!) {\n  deleteUser(id: $id) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    handle\n    profileS3Object\n  }\n}"

  public var id: String

  public init(id: String) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteUser", arguments: ["id": GraphQLVariable("id")], type: .object(DeleteUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteUser: DeleteUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteUser": deleteUser.flatMap { $0.snapshot }])
    }

    public var deleteUser: DeleteUser? {
      get {
        return (snapshot["deleteUser"] as? Snapshot).flatMap { DeleteUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteUser")
      }
    }

    public struct DeleteUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("handle", type: .nonNull(.scalar(String.self))),
        GraphQLField("profileS3Object", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, createdAt: Int, updatedAt: Int, handle: String, profileS3Object: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var handle: String {
        get {
          return snapshot["handle"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "handle")
        }
      }

      public var profileS3Object: String? {
        get {
          return snapshot["profileS3Object"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "profileS3Object")
        }
      }
    }
  }
}

public final class CreateUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateUser($createUserInput: CreateUserInput!) {\n  createUser(createUserInput: $createUserInput) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    handle\n    profileS3Object\n  }\n}"

  public var createUserInput: CreateUserInput

  public init(createUserInput: CreateUserInput) {
    self.createUserInput = createUserInput
  }

  public var variables: GraphQLMap? {
    return ["createUserInput": createUserInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUser", arguments: ["createUserInput": GraphQLVariable("createUserInput")], type: .object(CreateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createUser: CreateUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createUser": createUser.flatMap { $0.snapshot }])
    }

    public var createUser: CreateUser? {
      get {
        return (snapshot["createUser"] as? Snapshot).flatMap { CreateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createUser")
      }
    }

    public struct CreateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("handle", type: .nonNull(.scalar(String.self))),
        GraphQLField("profileS3Object", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, createdAt: Int, updatedAt: Int, handle: String, profileS3Object: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var handle: String {
        get {
          return snapshot["handle"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "handle")
        }
      }

      public var profileS3Object: String? {
        get {
          return snapshot["profileS3Object"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "profileS3Object")
        }
      }
    }
  }
}

public final class UpdateUserMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateUser($updateUserInput: UpdateUserInput!) {\n  updateUser(updateUserInput: $updateUserInput) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    handle\n    profileS3Object\n  }\n}"

  public var updateUserInput: UpdateUserInput

  public init(updateUserInput: UpdateUserInput) {
    self.updateUserInput = updateUserInput
  }

  public var variables: GraphQLMap? {
    return ["updateUserInput": updateUserInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUser", arguments: ["updateUserInput": GraphQLVariable("updateUserInput")], type: .object(UpdateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateUser: UpdateUser? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateUser": updateUser.flatMap { $0.snapshot }])
    }

    public var updateUser: UpdateUser? {
      get {
        return (snapshot["updateUser"] as? Snapshot).flatMap { UpdateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateUser")
      }
    }

    public struct UpdateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("handle", type: .nonNull(.scalar(String.self))),
        GraphQLField("profileS3Object", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, createdAt: Int, updatedAt: Int, handle: String, profileS3Object: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var handle: String {
        get {
          return snapshot["handle"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "handle")
        }
      }

      public var profileS3Object: String? {
        get {
          return snapshot["profileS3Object"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "profileS3Object")
        }
      }
    }
  }
}

public final class DeleteUserGameMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteUserGame($gameId: Int!) {\n  deleteUserGame(gameId: $gameId) {\n    __typename\n    userId\n    gameId\n  }\n}"

  public var gameId: Int

  public init(gameId: Int) {
    self.gameId = gameId
  }

  public var variables: GraphQLMap? {
    return ["gameId": gameId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteUserGame", arguments: ["gameId": GraphQLVariable("gameId")], type: .object(DeleteUserGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteUserGame: DeleteUserGame? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteUserGame": deleteUserGame.flatMap { $0.snapshot }])
    }

    public var deleteUserGame: DeleteUserGame? {
      get {
        return (snapshot["deleteUserGame"] as? Snapshot).flatMap { DeleteUserGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteUserGame")
      }
    }

    public struct DeleteUserGame: GraphQLSelectionSet {
      public static let possibleTypes = ["UserGame"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("gameId", type: .nonNull(.scalar(Int.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, gameId: Int) {
        self.init(snapshot: ["__typename": "UserGame", "userId": userId, "gameId": gameId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var gameId: Int {
        get {
          return snapshot["gameId"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "gameId")
        }
      }
    }
  }
}

public final class CreateUserGameMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateUserGame($createUserGameInput: CreateUserGameInput!) {\n  createUserGame(createUserGameInput: $createUserGameInput) {\n    __typename\n    userId\n    gameId\n  }\n}"

  public var createUserGameInput: CreateUserGameInput

  public init(createUserGameInput: CreateUserGameInput) {
    self.createUserGameInput = createUserGameInput
  }

  public var variables: GraphQLMap? {
    return ["createUserGameInput": createUserGameInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createUserGame", arguments: ["createUserGameInput": GraphQLVariable("createUserGameInput")], type: .object(CreateUserGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createUserGame: CreateUserGame? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createUserGame": createUserGame.flatMap { $0.snapshot }])
    }

    public var createUserGame: CreateUserGame? {
      get {
        return (snapshot["createUserGame"] as? Snapshot).flatMap { CreateUserGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createUserGame")
      }
    }

    public struct CreateUserGame: GraphQLSelectionSet {
      public static let possibleTypes = ["UserGame"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("gameId", type: .nonNull(.scalar(Int.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, gameId: Int) {
        self.init(snapshot: ["__typename": "UserGame", "userId": userId, "gameId": gameId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var gameId: Int {
        get {
          return snapshot["gameId"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "gameId")
        }
      }
    }
  }
}

public final class UpdateUserGameMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateUserGame($updateUserGameInput: UpdateUserGameInput!) {\n  updateUserGame(updateUserGameInput: $updateUserGameInput) {\n    __typename\n    userId\n    gameId\n  }\n}"

  public var updateUserGameInput: UpdateUserGameInput

  public init(updateUserGameInput: UpdateUserGameInput) {
    self.updateUserGameInput = updateUserGameInput
  }

  public var variables: GraphQLMap? {
    return ["updateUserGameInput": updateUserGameInput]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateUserGame", arguments: ["updateUserGameInput": GraphQLVariable("updateUserGameInput")], type: .object(UpdateUserGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateUserGame: UpdateUserGame? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateUserGame": updateUserGame.flatMap { $0.snapshot }])
    }

    public var updateUserGame: UpdateUserGame? {
      get {
        return (snapshot["updateUserGame"] as? Snapshot).flatMap { UpdateUserGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateUserGame")
      }
    }

    public struct UpdateUserGame: GraphQLSelectionSet {
      public static let possibleTypes = ["UserGame"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("gameId", type: .nonNull(.scalar(Int.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, gameId: Int) {
        self.init(snapshot: ["__typename": "UserGame", "userId": userId, "gameId": gameId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var gameId: Int {
        get {
          return snapshot["gameId"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "gameId")
        }
      }
    }
  }
}

public final class GetGameQuery: GraphQLQuery {
  public static let operationString =
    "query GetGame($id: Int!) {\n  getGame(id: $id) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    createdBy\n    startTime\n    endTime\n  }\n}"

  public var id: Int

  public init(id: Int) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getGame", arguments: ["id": GraphQLVariable("id")], type: .object(GetGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getGame: GetGame? = nil) {
      self.init(snapshot: ["__typename": "Query", "getGame": getGame.flatMap { $0.snapshot }])
    }

    public var getGame: GetGame? {
      get {
        return (snapshot["getGame"] as? Snapshot).flatMap { GetGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getGame")
      }
    }

    public struct GetGame: GraphQLSelectionSet {
      public static let possibleTypes = ["Game"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdBy", type: .nonNull(.scalar(String.self))),
        GraphQLField("startTime", type: .scalar(Int.self)),
        GraphQLField("endTime", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: Int, createdAt: Int, updatedAt: Int, createdBy: String, startTime: Int? = nil, endTime: Int? = nil) {
        self.init(snapshot: ["__typename": "Game", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: Int {
        get {
          return snapshot["id"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var createdBy: String {
        get {
          return snapshot["createdBy"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdBy")
        }
      }

      public var startTime: Int? {
        get {
          return snapshot["startTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: Int? {
        get {
          return snapshot["endTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "endTime")
        }
      }
    }
  }
}

public final class ListGamesQuery: GraphQLQuery {
  public static let operationString =
    "query ListGames {\n  listGames {\n    __typename\n    id\n    createdAt\n    updatedAt\n    createdBy\n    startTime\n    endTime\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listGames", type: .list(.object(ListGame.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listGames: [ListGame?]? = nil) {
      self.init(snapshot: ["__typename": "Query", "listGames": listGames.flatMap { $0.map { $0.flatMap { $0.snapshot } } }])
    }

    public var listGames: [ListGame?]? {
      get {
        return (snapshot["listGames"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { ListGame(snapshot: $0) } } }
      }
      set {
        snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "listGames")
      }
    }

    public struct ListGame: GraphQLSelectionSet {
      public static let possibleTypes = ["Game"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdBy", type: .nonNull(.scalar(String.self))),
        GraphQLField("startTime", type: .scalar(Int.self)),
        GraphQLField("endTime", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: Int, createdAt: Int, updatedAt: Int, createdBy: String, startTime: Int? = nil, endTime: Int? = nil) {
        self.init(snapshot: ["__typename": "Game", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: Int {
        get {
          return snapshot["id"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var createdBy: String {
        get {
          return snapshot["createdBy"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdBy")
        }
      }

      public var startTime: Int? {
        get {
          return snapshot["startTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: Int? {
        get {
          return snapshot["endTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "endTime")
        }
      }
    }
  }
}

public final class GetUserQuery: GraphQLQuery {
  public static let operationString =
    "query GetUser($id: String!) {\n  getUser(id: $id) {\n    __typename\n    id\n    createdAt\n    updatedAt\n    handle\n    profileS3Object\n  }\n}"

  public var id: String

  public init(id: String) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getUser", arguments: ["id": GraphQLVariable("id")], type: .object(GetUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getUser: GetUser? = nil) {
      self.init(snapshot: ["__typename": "Query", "getUser": getUser.flatMap { $0.snapshot }])
    }

    public var getUser: GetUser? {
      get {
        return (snapshot["getUser"] as? Snapshot).flatMap { GetUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getUser")
      }
    }

    public struct GetUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("handle", type: .nonNull(.scalar(String.self))),
        GraphQLField("profileS3Object", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, createdAt: Int, updatedAt: Int, handle: String, profileS3Object: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var handle: String {
        get {
          return snapshot["handle"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "handle")
        }
      }

      public var profileS3Object: String? {
        get {
          return snapshot["profileS3Object"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "profileS3Object")
        }
      }
    }
  }
}

public final class ListUsersQuery: GraphQLQuery {
  public static let operationString =
    "query ListUsers {\n  listUsers {\n    __typename\n    id\n    createdAt\n    updatedAt\n    handle\n    profileS3Object\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listUsers", type: .list(.object(ListUser.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listUsers: [ListUser?]? = nil) {
      self.init(snapshot: ["__typename": "Query", "listUsers": listUsers.flatMap { $0.map { $0.flatMap { $0.snapshot } } }])
    }

    public var listUsers: [ListUser?]? {
      get {
        return (snapshot["listUsers"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { ListUser(snapshot: $0) } } }
      }
      set {
        snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "listUsers")
      }
    }

    public struct ListUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("handle", type: .nonNull(.scalar(String.self))),
        GraphQLField("profileS3Object", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, createdAt: Int, updatedAt: Int, handle: String, profileS3Object: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var handle: String {
        get {
          return snapshot["handle"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "handle")
        }
      }

      public var profileS3Object: String? {
        get {
          return snapshot["profileS3Object"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "profileS3Object")
        }
      }
    }
  }
}

public final class GetUserGameQuery: GraphQLQuery {
  public static let operationString =
    "query GetUserGame($gameId: Int!) {\n  getUserGame(gameId: $gameId) {\n    __typename\n    userId\n    gameId\n  }\n}"

  public var gameId: Int

  public init(gameId: Int) {
    self.gameId = gameId
  }

  public var variables: GraphQLMap? {
    return ["gameId": gameId]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getUserGame", arguments: ["gameId": GraphQLVariable("gameId")], type: .object(GetUserGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getUserGame: GetUserGame? = nil) {
      self.init(snapshot: ["__typename": "Query", "getUserGame": getUserGame.flatMap { $0.snapshot }])
    }

    public var getUserGame: GetUserGame? {
      get {
        return (snapshot["getUserGame"] as? Snapshot).flatMap { GetUserGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getUserGame")
      }
    }

    public struct GetUserGame: GraphQLSelectionSet {
      public static let possibleTypes = ["UserGame"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("gameId", type: .nonNull(.scalar(Int.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, gameId: Int) {
        self.init(snapshot: ["__typename": "UserGame", "userId": userId, "gameId": gameId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var gameId: Int {
        get {
          return snapshot["gameId"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "gameId")
        }
      }
    }
  }
}

public final class ListUserGamesQuery: GraphQLQuery {
  public static let operationString =
    "query ListUserGames {\n  listUserGames {\n    __typename\n    userId\n    gameId\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listUserGames", type: .list(.object(ListUserGame.selections))),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listUserGames: [ListUserGame?]? = nil) {
      self.init(snapshot: ["__typename": "Query", "listUserGames": listUserGames.flatMap { $0.map { $0.flatMap { $0.snapshot } } }])
    }

    public var listUserGames: [ListUserGame?]? {
      get {
        return (snapshot["listUserGames"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { ListUserGame(snapshot: $0) } } }
      }
      set {
        snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "listUserGames")
      }
    }

    public struct ListUserGame: GraphQLSelectionSet {
      public static let possibleTypes = ["UserGame"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("gameId", type: .nonNull(.scalar(Int.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, gameId: Int) {
        self.init(snapshot: ["__typename": "UserGame", "userId": userId, "gameId": gameId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var gameId: Int {
        get {
          return snapshot["gameId"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "gameId")
        }
      }
    }
  }
}

public final class OnCreateGameSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateGame {\n  onCreateGame {\n    __typename\n    id\n    createdAt\n    updatedAt\n    createdBy\n    startTime\n    endTime\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateGame", type: .object(OnCreateGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateGame: OnCreateGame? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateGame": onCreateGame.flatMap { $0.snapshot }])
    }

    public var onCreateGame: OnCreateGame? {
      get {
        return (snapshot["onCreateGame"] as? Snapshot).flatMap { OnCreateGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateGame")
      }
    }

    public struct OnCreateGame: GraphQLSelectionSet {
      public static let possibleTypes = ["Game"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdBy", type: .nonNull(.scalar(String.self))),
        GraphQLField("startTime", type: .scalar(Int.self)),
        GraphQLField("endTime", type: .scalar(Int.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: Int, createdAt: Int, updatedAt: Int, createdBy: String, startTime: Int? = nil, endTime: Int? = nil) {
        self.init(snapshot: ["__typename": "Game", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "createdBy": createdBy, "startTime": startTime, "endTime": endTime])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: Int {
        get {
          return snapshot["id"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var createdBy: String {
        get {
          return snapshot["createdBy"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdBy")
        }
      }

      public var startTime: Int? {
        get {
          return snapshot["startTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "startTime")
        }
      }

      public var endTime: Int? {
        get {
          return snapshot["endTime"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "endTime")
        }
      }
    }
  }
}

public final class OnCreateUserSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateUser {\n  onCreateUser {\n    __typename\n    id\n    createdAt\n    updatedAt\n    handle\n    profileS3Object\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateUser", type: .object(OnCreateUser.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateUser: OnCreateUser? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateUser": onCreateUser.flatMap { $0.snapshot }])
    }

    public var onCreateUser: OnCreateUser? {
      get {
        return (snapshot["onCreateUser"] as? Snapshot).flatMap { OnCreateUser(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateUser")
      }
    }

    public struct OnCreateUser: GraphQLSelectionSet {
      public static let possibleTypes = ["User"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(String.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("updatedAt", type: .nonNull(.scalar(Int.self))),
        GraphQLField("handle", type: .nonNull(.scalar(String.self))),
        GraphQLField("profileS3Object", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: String, createdAt: Int, updatedAt: Int, handle: String, profileS3Object: String? = nil) {
        self.init(snapshot: ["__typename": "User", "id": id, "createdAt": createdAt, "updatedAt": updatedAt, "handle": handle, "profileS3Object": profileS3Object])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: String {
        get {
          return snapshot["id"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var createdAt: Int {
        get {
          return snapshot["createdAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var updatedAt: Int {
        get {
          return snapshot["updatedAt"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "updatedAt")
        }
      }

      public var handle: String {
        get {
          return snapshot["handle"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "handle")
        }
      }

      public var profileS3Object: String? {
        get {
          return snapshot["profileS3Object"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "profileS3Object")
        }
      }
    }
  }
}

public final class OnCreateUserGameSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateUserGame {\n  onCreateUserGame {\n    __typename\n    userId\n    gameId\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateUserGame", type: .object(OnCreateUserGame.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateUserGame: OnCreateUserGame? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateUserGame": onCreateUserGame.flatMap { $0.snapshot }])
    }

    public var onCreateUserGame: OnCreateUserGame? {
      get {
        return (snapshot["onCreateUserGame"] as? Snapshot).flatMap { OnCreateUserGame(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateUserGame")
      }
    }

    public struct OnCreateUserGame: GraphQLSelectionSet {
      public static let possibleTypes = ["UserGame"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("userId", type: .nonNull(.scalar(String.self))),
        GraphQLField("gameId", type: .nonNull(.scalar(Int.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(userId: String, gameId: Int) {
        self.init(snapshot: ["__typename": "UserGame", "userId": userId, "gameId": gameId])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var userId: String {
        get {
          return snapshot["userId"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "userId")
        }
      }

      public var gameId: Int {
        get {
          return snapshot["gameId"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "gameId")
        }
      }
    }
  }
}